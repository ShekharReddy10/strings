const numeric = [];
function string1(testString) {
    var flag = 0;
    for (let index in testString) {
        const checkValid = testString[index].split('.');
        if (checkValid[1] != undefined && checkValid[1].length > 2) {
            flag = 1;
        }
        else {
            const comma = testString[index].replace(",", "");
            numeric.push(Number(comma.replace("$", "")));
        }

    }
    if (flag === 0 && !isNaN ) {
        return numeric;
    } else {
        return 0;
    }
}

module.exports = string1;