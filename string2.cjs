function string2(testString){
    let expression = /^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/gm; 
    if (expression.test(testString)){
        const result =[];
        const array = testString.split('.');
        for(let index in array){
            result.push(Number(array[index]));
        }
        return result
    } 
    else{
        return [];
    }
}
module.exports = string2;