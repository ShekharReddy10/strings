Name = "";
function string4(testString) {
    for (let index in testString) {
        if (typeof(testString[index]) != 'string' || typeof(testString) != 'object') {
            return " ";
        }
        testString[index] = testString[index].charAt(0).toUpperCase() + testString[index].substr(1).toLowerCase();
        Name = Name + testString[index] + " ";
    }
    return Name;
}
module.exports = string4;